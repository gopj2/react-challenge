import React from "react";
import TimeInput from "./time-input.jsx";
import { render, fireEvent, waitForElement } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

describe("input component", () => {
  test("Matches the snapshot", () => {
    const { asFragment } = render(<TimeInput />);
    expect(asFragment()).toMatchSnapshot();
  });
});

describe("fill the input", () => {
  test("input.value should be 15 when we start change", () => {
    const { getByPlaceholderText } = render(<TimeInput />);
    let input = getByPlaceholderText(/place your time/i);
    fireEvent.change(input, { target: { value: 15 } });
    expect(input.value).toBe("15");
  });
});

describe("click on submit button", () => {
  test("expect clear the input after click on submit btn", () => {
    const onTimeInput = jest.fn();
    const { getByText, getByPlaceholderText } = render(
      <div>
        <TimeInput onTimeInput={onTimeInput} />
      </div>
    );
    let btn = getByText("Start");
    let input = getByPlaceholderText(/place your time/i);
    fireEvent.change(input, { target: { value: 15 } });
    fireEvent.click(btn);
    expect(input.value).toBe("");
  });
});
