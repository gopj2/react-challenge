import React, { Component } from "react";
import "./time-input.css";

export default class TimeInput extends Component {
  constructor() {
    super();
    this.state = {
      time: ""
    };
  }

  submitForm = e => {
    const { onTimeInput } = this.props;
    e.preventDefault();

    onTimeInput(this.state.time);
    this.setState({
      time: ""
    });
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.submitForm}>
          <input
            type="text"
            className="great-input"
            name="time"
            value={this.state.time}
            onChange={this.onChange}
            placeholder="Place your time"
            required
          ></input>
          <button className="start-countdown-btn" type="submit">
            Start
          </button>
        </form>
      </div>
    );
  }
}
