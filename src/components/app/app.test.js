import React from "react";
import { create } from "react-test-renderer";
import { render, fireEvent, waitForElement } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import App from "./app.jsx";

describe("App component", () => {
  test("Matches the snapshot", () => {
    const comp = create(<App />);
    expect(comp.toJSON()).toMatchSnapshot();
  });
});
