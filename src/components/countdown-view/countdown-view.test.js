import React from "react";

import CountDownView from "./countdown-view";
import {
  render,
  fireEvent,
  waitForElement,
  cleanup
} from "@testing-library/react";
import { act } from "react-dom/test-utils";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

describe("CountDownView component", () => {
  test("Matches the snapshot", () => {
    const { asFragment } = render(<CountDownView />);
    expect(asFragment()).toMatchSnapshot();
  });
});

describe("can not render label without right state", () => {
  test("no render label", () => {
    const { getByTestId } = render(<CountDownView />);
    const div = getByTestId(/div_test/i);
    expect(div).toHaveTextContent("");
  });
});

describe("render label 'More than halfway there' with right state", () => {
  test("render label 'More than halfway there'", () => {
    const timerTime = 1;
    const timerStart = 10;
    const ended = false;
    const showlabel = true;
    const { getByText } = render(
      <CountDownView
        timerTime={timerTime}
        timerStart={timerStart}
        ended={ended}
        showlabel={showlabel}
      />
    );
    const labelDiv = getByText(/More than halfway there!/i);
    expect(labelDiv).toHaveTextContent("More than halfway there!");
  });
});

describe("render label 'Time`s up!' with right state", () => {
  test("render label 'Time`s up!'", () => {
    const ended = true;

    const { getByText } = render(<CountDownView ended={ended} />);
    const labelDiv = getByText(/Time’s up!/i);
    expect(labelDiv).toHaveTextContent("Time’s up!");
  });
});

describe("render time when we get it from the props ", () => {
  test("render time", () => {
    const mins = "10";
    const secs = "23";

    const { getByText } = render(<CountDownView mins={mins} secs={secs} />);
    const minsDiv = getByText(/10/i);
    const secsDiv = getByText(/23/i);
    expect(minsDiv).toHaveTextContent("10");
    expect(secsDiv).toHaveTextContent("23");
  });
});

describe("render resume and reset button", () => {
  test("it should render them when state is right", () => {
    const timerOn = false;
    const timerTime = 1;
    const timerStart = 5;
    const { getByText } = render(
      <CountDownView
        timerOn={timerOn}
        timerTime={timerTime}
        timerStart={timerStart}
      />
    );
    const btn1 = getByText(/Resume/i);
    const btn2 = getByText(/Reset/i);
    expect(btn1).toBeDefined();
    expect(btn2).toBeDefined();
  });
});

describe("click on resume button expect to call function from props", () => {
  test("it should call parent function", () => {
    const startTimer = jest.fn();
    const timerOn = false;
    const timerTime = 1;
    const timerStart = 1;
    const mins = "01";
    const secs = "00";
    const { getByText } = render(
      <CountDownView
        timerOn={timerOn}
        timerTime={timerTime}
        mins={mins}
        secs={secs}
        startTimer={startTimer}
      />
    );
    const btn1 = getByText(/Resume/i);
    fireEvent.click(btn1);
    expect(startTimer.mock.calls.length).toBe(1);
  });
});
