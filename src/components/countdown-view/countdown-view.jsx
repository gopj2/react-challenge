import React, { Component } from "react";
import "./countdown-view.css";

export default class CountDownView extends Component {
  //showLabel pass to parent for changing state after end

  render() {
    const {
      timerTime,
      timerStart,
      timerOn,
      mins,
      secs,
      startTimer,
      resetTimer,
      stopTimer,
      ended
    } = this.props;

    let classColor = "green-text";
    if (timerTime <= 20000) {
      classColor = "red-text";
    }
  
    return (
      <div>
        <div className="Countdown-time">
          <div data-testid="div_test" className="label">
            {timerTime < timerStart / 2 &&
              ended === false &&
              this.props.showlabel === true && (
                <div className={classColor}>More than halfway there!</div>
              )}

            {ended === true && <div className={classColor}>Time’s up!</div>}
          </div>
          <div className="time-block">
            <div className="mins" data-testid="1">
              {mins}
            </div>
            <div className="delimeter">:</div>
            <div className="secs">{secs}</div>
          </div>
        </div>

        {timerOn === true && timerTime >= 1000 && (
          <button className="control-btn" onClick={stopTimer}>
            Pause
          </button>
        )}
        {timerOn === false && timerTime !== 0 && (
          <button className="control-btn resume-btn" onClick={startTimer}>
            Resume
          </button>
        )}
        {(timerOn === false || timerTime < 1000) && timerStart > 0 && (
          <button className="control-btn reset-btn" onClick={resetTimer}>
            Reset
          </button>
        )}
      </div>
    );
  }
}
