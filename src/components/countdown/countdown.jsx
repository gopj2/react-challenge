import React, { Component } from "react";
import TimeInput from "../time-input";
import SpeedButtons from "../speed-buttons";
import CountDownView from "../countdown-view";
import Sound from "react-sound";
import "./countdown.css";

export default class CountDown extends Component {
  constructor() {
    super();
    this.state = {
      timerOn: false,
      timerStart: 0,
      timerTime: 0,
      speedOfTimer: 1000,
      ended: false,
      showlabel: true,
      audio: false
    };
  }

  startTimer = () => {
    if (this.state.ended) {
      return;
    }

    if (this.state.timerOn === false) {
      this.setState({
        timerOn: true,
        timerTime: this.state.timerTime,
        ended: false,
        audio: false
      });
    }

    this.timer = setInterval(() => {
      const newTime = this.state.timerTime - 1000;
      if (newTime >= 0) {
        this.setState({
          timerTime: newTime
        });
      } else {
        clearInterval(this.timer);
        this.setState({ timerOn: false, ended: true, audio: true });
      }
    }, this.state.speedOfTimer);
  };

  stopTimer = () => {
    clearInterval(this.timer);
    this.setState({ timerOn: false });
  };

  resetTimer = () => {
    if (this.state.timerOn === false) {
      this.setState({
        timerTime: 0,
        timerStart: 0,
        ended: false,
        speedOfTimer: 1000,
        showlabel: true,
        audio: false
      });
      clearInterval(this.timer);
      clearInterval(this.blink);
    }
  };

  onTimeInput = time => {
    if (
      (this.state.timerTime !== 0 && this.state.timerStart !== 0) ||
      this.state.ended
    ) {
      return;
    }

    this.setState({ timerTime: time * 60000, timerStart: time * 60000 }, () => {
      this.startTimer();
    });
  };

  ChangeSpeed = value => {
    if (this.state.timerOn !== false) {
      this.setState({ speedOfTimer: value }, () => {
        clearInterval(this.timer);
        this.startTimer();
      });
    }
  };

  blinker = () => {
    let sLb = !this.state.showlabel;
    this.setState({ showlabel: sLb });
  };

  render() {
    const { timerTime, timerStart, timerOn, ended, showlabel } = this.state;
    let seconds = ("0" + (Math.floor((timerTime / 1000) % 60) % 60)).slice(-2);
    let minutes = ("0" + Math.floor(timerTime / 60000)).slice(-2);

    if (timerTime === 10000 && ended === false && timerOn) {
      this.blink = setInterval(() => {
        this.blinker();
      }, 1000);
    }

    if (ended) {
      clearInterval(this.blink);
    }

    const sound =
      "https://api.coderrocketfuel.com/assets/pomodoro-times-up.mp3";

    return (
      <div className="container">
        <div className="countdown">
          <TimeInput onTimeInput={this.onTimeInput} />
          <CountDownView
            timerTime={timerTime}
            timerStart={timerStart}
            timerOn={timerOn}
            mins={minutes}
            secs={seconds}
            ended={ended}
            stopTimer={this.stopTimer}
            resetTimer={this.resetTimer}
            startTimer={this.startTimer}
            showlabel={showlabel}
          />
          <SpeedButtons onClicked={this.ChangeSpeed} timerOn={timerOn} />
          {this.state.audio === true && (
            <Sound
              url={sound}
              playStatus={Sound.status.PLAYING}
              onLoading={this.handleSongLoading}
              onPlaying={this.handleSongPlaying}
              onFinishedPlaying={this.handleSongFinishedPlaying}
            />
          )}
        </div>
      </div>
    );
  }
}
