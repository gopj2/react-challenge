import React from "react";
import CountDown from "./countdown";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

describe("CountDown component", () => {
  test("Matches the snapshot", () => {
    const { asFragment } = render(<CountDown />);
    expect(asFragment()).toMatchSnapshot();
  });
});
