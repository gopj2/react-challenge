import React from "react";
import { render, fireEvent, waitForElement } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import "@testing-library/jest-dom/extend-expect";

import SpeedButtons from "./speed-buttons.jsx";
describe("SpeedButtons component", () => {
  test("Matches the snapshot", () => {
    const { asFragment } = render(<SpeedButtons />);
    expect(asFragment()).toMatchSnapshot();
  });
});

describe("can not render without timerOn === true", () => {
  test("render without timerOn === true", () => {
    const timerOn = false;
    const { getByTestId } = render(<SpeedButtons timerOn={timerOn} />);
    const div = getByTestId(/speed_buttons/i);
    expect(div.firstChild).toBe(null);
  });
});

describe("call parent functions after click on the speed button", () => {
  test("number of clicks", () => {
    const onClicked = jest.fn();
    const timerOn = true;
    const { getByText } = render(
      <SpeedButtons onClicked={onClicked} timerOn={timerOn} />
    );
    const btn1 = getByText("1x");
    const btn2 = getByText("1.5x");
    const btn3 = getByText("2x");
    fireEvent.click(btn1);
    fireEvent.click(btn2);
    fireEvent.click(btn3);
    expect(onClicked.mock.calls.length).toBe(3);
  });
});
