import React, { Component } from "react";
import "./speed-buttons.css";

export default class SpeedButtons extends Component {
  passValue = e => {
    const { onClicked } = this.props;
    onClicked(e.target.value);
  };

  render() {
    const { timerOn } = this.props;
    return (
      <div data-testid="speed_buttons">
        {timerOn === true && (
          <div>
            <button className="speed-btn" value="1000" onClick={this.passValue}>
              1x
            </button>
            <button className="speed-btn" value="850" onClick={this.passValue}>
              1.5x
            </button>
            <button className="speed-btn" value="500" onClick={this.passValue}>
              2x
            </button>
          </div>
        )}
      </div>
    );
  }
}
